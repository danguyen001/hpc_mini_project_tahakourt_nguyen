from mpi4py import MPI
import numpy as np
from PIL import Image
import operator
# import scorep
from gaussian_blur import gaussian_blur
from numpy.testing import assert_array_almost_equal


def get_img_channels(img):
    if img.ndim > 2:
        return image.shape[2]
    else:
        return 1


def check(mpi_result, cols, rows, channels):
    sequentielle_result = np.loadtxt('blurred_img.txt')
    sequentielle_result = np.reshape(sequentielle_result, (rows, cols, channels))

    # if mismatch < 1% then pass the test
    diff = np.abs(sequentielle_result - mpi_result)
    mismatch_percent = np.mean(diff) * 100 / np.mean(mpi_result)

    if mismatch_percent < 0.01:
        return "OK - Mismatch percent: " + str(mismatch_percent)
    return "Not OK - Mismatch percent: " + str(mismatch_percent)


# with scorep.instrumenter.enable():
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

start_time = MPI.Wtime()

# read image from file
if rank == 0:
    image = Image.open("Lena1080.png")
    image = np.array(image)
    rows = image.shape[0]
    cols = image.shape[1]
    channels = get_img_channels(image)
    displacements = None
else:
    image = None
    rows = None
    cols = None
    channels = None
    displacements = None

# broadcast image info to all processes
rows = comm.bcast(rows, root=0)
cols = comm.bcast(cols, root=0)
image = comm.bcast(image, root=0)
channels = comm.bcast(channels, root=0)
matrix_size = 3

local_img = image[rank * rows // size:(rank + 1) * rows // size]

blurred_local_img = np.zeros(local_img.shape, dtype=np.uint8)
if channels > 1:
    for i in range(3):
        blurred_local_img[:, :, i] = gaussian_blur(org_image=local_img[:, :, i].astype(int), box_blur_size=matrix_size)
else:
    blurred_local_img = np.array(gaussian_blur(org_image=local_img.astype(int), box_blur_size=matrix_size), dtype=np.uint8)

local_size = len(blurred_local_img.flatten())

comm.barrier()
sizes = comm.gather(local_size, root=0)
if rank == 0:
    displacements = [0] + list(np.cumsum(sizes[:-1]))
displacements = comm.bcast(displacements, root=0)

# comm.Gather(blurred_local_img, image)
comm.Gatherv(blurred_local_img.flatten(), [image, sizes, displacements, MPI.BYTE], root=0)

end_time = MPI.Wtime()
if rank == 0:
    # write execution time to file
    execution_time = end_time - start_time
    print(execution_time)
    # with open("result_with_pythran1.dat", 'a') as f:
    #	f.write(f"{size} {execution_time}\n")

    # save the image

    if channels > 1:
        blurred_img = np.reshape(image, (rows, cols, channels))
    else:
        blurred_img = np.reshape(image, (rows, cols))
    check_result = check(blurred_img, cols, rows, channels)
    print("size: ", size, "-result: ", check_result)
    Image.fromarray(np.uint8(blurred_img)).save("Lena1080_blurred_image_3.png")
