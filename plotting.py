import matplotlib.pyplot as plt


def create_plot(input_file, x_label, y_label, output_file):
    list1 = []
    time_list = []
    with open(input_file, "r") as file:
        for l in file:
            l = l.strip()
            data = l.split(" ")
            list1.append(float(data[0]))
            time_list.append(float(data[1]))

    fig, ax = plt.subplots()

    # ax.loglog(list1, time_list)
    ax.plot(list1, time_list, marker='o')

    for i, txt in enumerate(time_list):
        ax.annotate("{:.2f}".format(txt), (list1[i], time_list[i]))

    ax.set(xlabel=x_label, ylabel=y_label)
    plt.savefig(output_file)


# Plot version pythran
create_plot("result_with_pythran1.dat", "Nombre de processus", "Temp d'execution(s)", "blur_img_performance_nombre_processus_pythran.pdf")
create_plot("result_with_pythran2.dat", "Taille de matrice", "Temp d'execution(s)", "blur_img_performance_taille_matrice_pythran.pdf")

# Plot version without pythran
create_plot("result_without_pythran1.dat", "Nombre de processus", "Temp d'execution(s)", "blur_img_performance_nombre_processus.pdf")
create_plot("result_without_pythran2.dat", "Taille de matrice", "Temp d'execution(s)", "blur_img_performance_taille_matrice.pdf")
