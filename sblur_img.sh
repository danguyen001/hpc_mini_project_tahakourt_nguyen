#!/usr/bin/env bash
#SBATCH -N 1
#SBATCH -n 20
#SBATCH -c 1


module load scalasca
for ((i=4; i<=4; i*=2))
do
	mpirun -np $i python3 blur_img_mpi.py
done
