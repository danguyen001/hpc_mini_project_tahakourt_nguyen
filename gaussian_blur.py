import numpy as np


# pythran export gaussian_blur(int[][], int)
def gaussian_blur(org_image, box_blur_size):
    box_blur_matrix = np.ones((box_blur_size, box_blur_size)) / (box_blur_size * box_blur_size)
    radius = box_blur_size // 2
    org_img_rows = org_image.shape[0]
    org_img_cols = org_image.shape[1]

    blurred_image = np.zeros(org_image.shape)
    padded_rows = org_img_rows + box_blur_size - 1
    padded_cols = org_img_cols + box_blur_size - 1

    img_with_pad = np.zeros((padded_rows, padded_cols))
    img_with_pad[radius:-radius, radius:-radius] = org_image  # center
    img_with_pad[:radius, radius:-radius] = org_image[:radius, :]  # top rows
    img_with_pad[-radius:, radius:-radius] = org_image[-radius:, :]  # bottom rows
    img_with_pad[radius:-radius, :radius] = org_image[:, :radius]  # left cols
    img_with_pad[radius:-radius, -radius:] = org_image[:, -radius:]  # right cols

    for x in range(org_img_rows):
        for y in range(org_img_cols):
            x_start_index = x
            x_end_index = x + box_blur_size

            y_start_index = y
            y_end_index = y + box_blur_size

            blurred_image[x, y] = (box_blur_matrix * img_with_pad[x_start_index: x_end_index, y_start_index:y_end_index]).sum()

    return blurred_image
