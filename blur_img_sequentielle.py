import numpy as np
from PIL import Image
from gaussian_blur import gaussian_blur


def get_img_channels(img):
    if img.ndim > 2:
        return image.shape[2]
    else:
        return 1


# read image from file
image = Image.open("Lena1080.png")
image = np.array(image)
rows = image.shape[0]
cols = image.shape[1]
channels = get_img_channels(image)

matrix_size = 3

blurred_img = np.zeros(image.shape, dtype=np.uint8)
if channels > 1:
    for i in range(3):
        # blurred_img[:, :, i] = gaussian_blur(org_image=image[:, :, i], box_blur_size=matrix_size)
        blurred_img[:, :, i] = gaussian_blur(org_image=image[:, :, i].astype(int), box_blur_size=matrix_size)
else:
    blurred_img = np.array(gaussian_blur(org_image=image, box_blur_size=matrix_size), dtype=np.uint8)

# save the image
Image.fromarray(np.uint8(blurred_img)).save("Lena1080_blurred_image.png")

# write the result array to file
blurred_img_flattened = blurred_img.reshape(-1)
np.savetxt("blurred_img.txt", blurred_img_flattened, fmt="%d")
print("done")
