# HPC Project #4 - Implémenter un box filter (Gaussian blur) sur une image

Membres :
- Duc Anh NGUYEN - M2 Big Data
- Maya TAHAKOURT - M2 Big Data

Le rapport est déposé sur Elearn

## Fichiers du projet
- `blur_img_mpi.py` : Effectuer un flou d'image en parallèle en utilisant MPI
- `blur_img_sequentielle.py` : Effectuer le flou d'image de manière séquentielle
- `gaussian_blur.py` : Contient l'algorithme Gaussian Blur
- `plotting.py` : Créer le graphe
- `blurred_img.txt` : Contient le résultat de la version séquentielle, utilisé pour comparer avec le résultat de la version MPI



